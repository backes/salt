#!/bin/bash

unset PYTHONPATH
conda activate /eos/user/b/backes/.conda/envs/salt

cd /eos/user/b/backes/QT/salt/salt

salt fit --config configs/Nett_hier.yaml --force
