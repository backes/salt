import h5py
import numpy as np
from puma import Histogram, HistogramPlot, Roc, RocPlot
from puma.metrics import calc_rej
from puma.utils import get_good_colours, get_good_linestyles, logger
import matplotlib.pyplot as plt

networks = {
    "Dipz" : "/eos/user/b/backes/QT/salt/salt/logs/Dipz_20231103-T103513/ckpts/epoch=039-val_loss=-2.59090__test_ttbar.h5"
}
reference = "Dipz"

# networks = {
#     "Hitz" : "/eos/user/b/backes/QT/salt/salt/logs/Hitz_20231102-T171418/ckpts/epoch=064-val_loss=-0.03452__test_ttbar.h5"
# }
# reference = "Hitz"

test_path = '/eos/user/b/backes/QT/preprocessing/output/pp_output_test_ttbar.h5'
num_jets = 9000


# load test data

logger.info("Load data")
with h5py.File(test_path, 'r') as test_f:
    jets = test_f['jets'][:num_jets]
    jet_z = jets['TruthJetPVz']
    eta = jets['eta']
    #print(test_f.keys())
    #print(jets.dtype)
    mask = (jet_z < 200) & (jet_z > -200)


for key, val in networks.items():
    with h5py.File(val, 'r') as f:
        jets = f['jets'][:num_jets]
        print(jets.dtype.names)
        #print(jets.shape)
        #print(jets)
        #jet_z_hitz = jets[:,0]
        jet_z_hitz = jets["TruthJetPVz_gaussian_regression"]
        # jet_z_hitz = jets["TruthJetPVz_regression_with_mean_std_norm"]
        
        print(jet_z_hitz.shape)
        print(jet_z_hitz)
        #print(jet_z_hitz[:,0])

jet_z_plot = HistogramPlot(
    bins_range=(-200, 200),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)

jet_z_plot.add(
    Histogram(
        jet_z.flatten(), label="True"
    )
)

jet_z_plot.add(
        #Histogram(jet_z_hitz.flatten(), label="Hitz")
        Histogram(jet_z_hitz[:,0].flatten(), label=reference)
)


# print(jet_z_hitz)
# print(jet_z_hitz[:,0])
jet_z_plot.draw()
jet_z_plot.savefig(reference + "/jet_z.png")

print(jet_z-jet_z_hitz[:,0])






jet_diff = jet_z-jet_z_hitz[:,0]

jet_z_plot2 = HistogramPlot(
    bins_range=(-200, 200),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)

jet_z_plot2.add(
    Histogram(
        jet_z.flatten(), label="True position $z_t$"
    )
)

jet_z_plot2.add(
        Histogram(
            jet_diff.flatten(), label = reference + " $z_t-z_p$"
            )
)


jet_z_plot2.draw()
jet_z_plot2.savefig(reference + "/diff_jet_z.png")




jet_norm = abs(jet_z-jet_z_hitz[:,0])/jet_z_hitz[:,1]

jet_z_plot3 = HistogramPlot(
    bins_range=(0, 50),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)

jet_z_plot3.add(
        Histogram(
            jet_norm.flatten(), label = reference + r" $(z_t-z_p)/\sigma_p$"
            )
)

jet_z_plot3.draw()
jet_z_plot3.savefig(reference + "/diff_divided.png")




jet_z_plot4 = HistogramPlot(
    bins_range=(0, 5),
    xlabel="$z$ Position",
    ylabel="Normalised number of jets",
    figsize=(6, 4.5),
)

jet_z_plot4.add(
        Histogram(
            jet_norm.flatten(), label = reference + r" $(z_t-z_p)/\sigma_p$"
            )
)

jet_z_plot4.draw()
jet_z_plot4.savefig(reference + "/diff_divided_zoomed.png")







def Gauss(x, mu, sigma):
    return 1/np.sqrt(2*np.pi*sigma**2) * np.exp(-(x-mu)**2/(2*sigma**2))

plt.figure(figsize=(12,8))
x = np.linspace(-150,150, 100000)
for i in range(10):
    y = Gauss(x, jet_z_hitz[i,0], jet_z_hitz[i,1])
    plt.plot(x[y>0.01], y[y>0.01]/np.max(y), label = "$z_t=${:0.3f}, $z_p=${:0.3f}, $\sigma_p=${:0.3f}".format(jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1],2))
    plt.vlines(jet_z[i], 0, 1, color='red', ls = '--')
    

    print(i, jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1])

plt.xlabel("$z$ Position", fontsize=20)
plt.ylabel("Normalised unit", fontsize=20)
plt.ylim(0,2)
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.legend(loc="upper left", fontsize=15)
plt.savefig(reference + "/Single_events.png", format='png')






# plt.figure(figsize=(12,8))
# x = np.linspace(-150,150, 100000)
# for i in range(1000):

#     if abs(jet_z[i]-jet_z_hitz[i,0]) > 10:
#         y = Gauss(x, jet_z_hitz[i,0], jet_z_hitz[i,1])
#         #plt.plot(x[y>0.00001], y[y>0.00001]/np.max(y), label = "$z_t=${:0.3f}, $z_p=${:0.3f}, $\sigma_p=${:0.3f}".format(jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1],2))
#         plt.vlines(jet_z[i], 0, 1, color='red', ls = '--')
        

#         print(i, jet_z[i],jet_z_hitz[i,0], jet_z_hitz[i,1])

# plt.xlabel("$z$ Position", fontsize=20)
# plt.ylabel("Normalised unit", fontsize=20)
# plt.ylim(0,2)
# plt.xticks(fontsize=20)
# plt.yticks(fontsize=20)
# plt.legend(loc="upper left", fontsize=15)
# plt.savefig("Single_events_worst_case.png", format='png')


# # load test data
# logger.info("Load data")
# with h5py.File(test_path, 'r') as test_f:
#     jets = test_f['jets'][:num_jets]
#     jet_pt = jets['pt'] / 1000
#     jet_mass = jets['mass'] / 1000
#     jet_eta = np.abs(jets['eta'])
#     flav = jets['R10TruthLabel_R22v1']
#     mask = (jet_pt < 1000) & (jet_pt > 250) & (jet_mass > 50) & (jet_mass < 300)
#     is_QCD = flav == 10
#     is_Hcc = flav == 12
#     is_Hbb = flav == 11
#     is_Top = flav == 1
#     n_jets_QCD = np.sum(is_QCD & mask)
#     n_jets_Top = np.sum(is_Top & mask)

# results = {}
# logger.info("Calculate rejections")
# for key, val in networks.items():
#     with h5py.File(val, 'r') as f:
#         jets = f['jets'][:num_jets]
#         pHbb = jets[f'{key}_phbb']
#         pHcc = jets[f'{key}_phcc']
#         pQCD = jets[f'{key}_pqcd']
#         pTop = jets[f'{key}_ptop']
#         disc_Hbb = pHbb
#         disc_Hcc = pHcc

#         sig_eff = np.linspace(0.4, 1, 100)
#         Hbb_rej_QCD = calc_rej(disc_Hbb[is_Hbb & mask], disc_Hbb[is_QCD & mask], sig_eff)
#         Hbb_rej_Top = calc_rej(disc_Hbb[is_Hbb & mask], disc_Hbb[is_Top & mask], sig_eff)
#         Hcc_rej_QCD = calc_rej(disc_Hcc[is_Hcc & mask], disc_Hcc[is_QCD & mask], sig_eff)
#         Hcc_rej_Top = calc_rej(disc_Hcc[is_Hcc & mask], disc_Hcc[is_Top & mask], sig_eff)
#         results[key] = {
#             'sig_eff' : sig_eff,
#             'disc_Hbb' : disc_Hbb,
#             'disc_Hcc' : disc_Hcc,
#             'Hbb_rej_QCD' : Hbb_rej_QCD,
#             'Hbb_rej_Top' : Hbb_rej_Top,
#             'Hcc_rej_QCD' : Hcc_rej_QCD,
#             'Hcc_rej_Top' : Hcc_rej_Top
#         }

# logger.info("Plotting Discriminants.")
# plot_histo = {
#     key : HistogramPlot(
#         n_ratio_panels=1,
#         ylabel="Normalised number of jets",
#         xlabel=f"{key}-jet discriminant",
#         logy=True,
#         leg_ncol=1,
#         figsize=(6.5, 4.5),
#         bins=np.linspace(0, 1, 50),
#         y_scale=1.5,
#         atlas_second_tag="$\\sqrt{s}=13$ TeV, Xbb jets",
#     ) for key in ['Hbb', 'Hcc']}
# linestyles = get_good_linestyles()[:len(networks.keys())]
# colours = get_good_colours()[:3]
# for key, value in plot_histo.items():
#     for network, linestyle in zip(networks.keys(), linestyles):
#         value.add(
#             Histogram(
#                 results[network][f'disc_{key}'][is_QCD],
#                 label="QCD jets" if network == reference else None,
#                 ratio_group="QCD",
#                 colour=colours[0],
#                 linestyle=linestyle,
#             ),
#             reference=(network == reference),
#             )
#         value.add(
#             Histogram(
#                 results[network][f'disc_{key}'][is_Top],
#                 label="Top jets" if network == reference else None,
#                 ratio_group="Top",
#                 colour=colours[1],
#                 linestyle=linestyle,
#             ),
#             reference=(network == reference),
#             )
#         value.add(
#             Histogram(
#                 results[network][f'disc_{key}'][is_Hbb if key == 'Hbb' else is_Hcc],
#                 label=f"{key} jets" if network == reference else None,
#                 ratio_group=f"{key}",
#                 colour=colours[2],
#                 linestyle=linestyle,
#             ),
#             reference=(network == reference),
#             )
#     value.draw()
#     # The lines below create a legend for the linestyles
#     value.make_linestyle_legend(
#         linestyles=linestyles, labels=networks.keys(), bbox_to_anchor=(0.5, 1)
#     )
#     value.savefig(f"disc_{key}.png", transparent=False)

# # here the plotting of the roc starts
# logger.info("Plotting ROC curves.")
# plot_roc = {
#     key : RocPlot(
#         n_ratio_panels=2,
#         ylabel="Background rejection",
#         xlabel=f"{key}-jet efficiency",
#         atlas_second_tag="$\\sqrt{s}=13$ TeV, Xbb jets",
#         figsize=(6.5, 6),
#         y_scale=1.4,
#     ) for key in ['Hbb', 'Hcc']}

# for key, value in plot_roc.items():
#     for network in networks.keys():
#         value.add_roc(
#             Roc(
#                 sig_eff,
#                 results[network][f'{key}_rej_QCD'],
#                 n_test=n_jets_QCD,
#                 rej_class="qcd",
#                 signal_class=f"{key}",
#                 label=f"{network}",
#             ),
#             reference=(reference == network),
#         )
#         value.add_roc(
#             Roc(
#                 sig_eff,
#                 results[network][f'{key}_rej_Top'],
#                 n_test=n_jets_Top,
#                 rej_class="top",
#                 signal_class=f"{key}",
#                 label=f"{network}",
#             ),
#             reference=(reference == network),
#         )
#     # setting which flavour rejection ratio is drawn in which ratio panel
#     value.set_ratio_class(1, "qcd")
#     value.set_ratio_class(2, "top")
#     value.draw()
#     value.savefig(f"roc_{key}.png", transparent=False)
